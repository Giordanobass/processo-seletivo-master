var inicio = new Vue({
	el:"#lista-fabricantes",
    data: {
        listaFabricantes: [],
        listaFabricantesHeader: [
			{sortable: false, key: "nome", label:"Nome"}
		]
    },
    
    created: function(){
        let vm =  this;
        vm.buscarFabricantes();
    },
    
    methods:{
		buscarFabricantes: function(){
			const vm = this;
			axios.get("/mercado/rs/fabricantes")
			.then(response => {
				vm.listaFabricantes = response.data;
			}).catch(function (error) {
				alert('erro');
			}).finally(function() {
			});
		},
		
		excluirFabricante: function(id){
			const vm = this;
			axios.delete("/mercado/rs/fabricantes/" + id)
			.then( () => {
				alert('sucesso');
				vm.buscarFabricantes(); 
			}).catch(function (error) { // erro chave estrangeira 
				alert('erro');
			}).finally(function() {
			});
		},
    }
});
