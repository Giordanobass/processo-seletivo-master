# Requisitos

1. Java 8
2. Apache Tomcat 9
3. MySql 5 
4. IDE Eclipse

# O que fazer agora

-No mySql criar um schema 'mercado-bd'
-Importar o projeto no eclipse
	 -A aplica��o est� configurada com usu�rio: 'root' e senha: 'giordanob2'
	 - para alterar use o arquivo persistence.xml na linha: <property name="javax.persistence.jdbc.password" value="giordanob2" /> 
-Criar um servidor do Tomcat 8 no eclipse
-Adicionar o projeto no servidor do tomcat
-Iniciar o servidor do tomcat 
-Acessar a pagina pelo endere�o http://localhost:8080/mercado/
