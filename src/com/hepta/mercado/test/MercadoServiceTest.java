package com.hepta.mercado.test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class MercadoServiceTest {

	private static WebTarget serviceProd;
	private static WebTarget serviceFab;
	private static final String URL_LOCAL = "http://localhost:8080/mercado/rs/produtos";
	private static final String URL_LOCAL2 = "http://localhost:8080/mercado/rs/fabricantes";
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		serviceProd = client.target( UriBuilder.fromUri(URL_LOCAL).build() );
		serviceFab = client.target( UriBuilder.fromUri(URL_LOCAL2).build() );
		
	}

	@Test
	void testListaTodosProdutos() {
		// QUANDO
		Response response = serviceProd.request().get();
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}
	
	@Test
	void testListaTodosFabricantes() {
		// QUANDO
		Response response = serviceFab.request().get();
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

}
