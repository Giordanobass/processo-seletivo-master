var inicio = new Vue({
	el: "#inicio",
	data: {
		listaProdutos: [],
		listaProdutosHeader: [
			{ sortable: false, key: "nome", label: "Nome" },
			{ sortable: false, key: "fabricante.nome", label: "Fabricante" },
			{ sortable: false, key: "volume", label: "Volume" },
			{ sortable: false, key: "unidade", label: "Unidade" },
			{ sortable: false, key: "estoque", label: "Estoque" }
		],

	},

	created: function () {
		let vm = this;
		vm.buscarProdutos();

	},

	methods: {

		buscarProdutos: function () {
			const vm = this;
			axios.get("/mercado/rs/produtos")
				.then(response => {
					vm.listaProdutos = response.data;
				}).catch(function (error) {
					alert('erro');
				}).finally(function () {
				});
		},


		excluirProduto: function (id) {
			const vm = this;
			axios.delete('/mercado/rs/produtos/' + id) 
				.then(() => {alert('sucesso');
					
					vm.buscarProdutos(); 
					alert('erro');
				}).finally(function () {
				});
		},

		editarProduto: function (id) {
			console.log('id: ' + id)
		}

	}
});