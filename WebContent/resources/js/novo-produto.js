var inicio = new Vue({
	el: "#novo-produto",
	data: {
		produto: {},
		listaFabricantes: [],
		fabricante: "",

	},

	created: function () {
		let vm = this;
		vm.buscarFabricantes();

	},

	methods: {

		buscarFabricantes: function () {
			const vm = this;
			axios.get("/mercado/rs/fabricantes").then(response => {
				vm.listaFabricantes = response.data;
			}).catch(function (_error) {
			}).finally(function () {
			});
		},

		salvarProduto: function () {
			const vm = this;

			vm.listaFabricantes.forEach(f => {
				if (f.nome == vm.fabricante) {
					vm.produto.fabricante = f;
				}
			});

			axios.post("/mercado/rs/produtos", vm.produto).then(
				alert('sucesso')
			).catch(function (_error) {
				alert('erro')
			}).finally(function () {
			});
		},

		editarProduto: function (id, _produto) {
			const vm = this;
			axios.put("/mercado/rs/produtos/" + id, vm.produto
			).then(response => {
				alert(response.data);
				vm.buscaProdutos();
			}).catch(function (_error) {
				alert('erro');
			}).finally(function () {

			});
		},

		buscaPorId: function (id) {
			const vm = this;
			console.log('entrei');
			axios.get("/mercado/rs/produtos/" + id).then(response => {
				vm.produto = response.data;
			}).catch(function (_error) {
			}).finally(function () { });
		},
	}
});